def remove_outlier(data, listColOutliers):
  for col in listColOutliers:
    Q1 = np.percentile(dataMobile_noDuplicate[col], 25, interpolation = 'midpoint')
    Q3 = np.percentile(dataMobile_noDuplicate[col], 75, interpolation = 'midpoint')
    IQR = Q3 - Q1
    RUB = Q3 + (1.5*IQR)
    RLB = Q1 - (1.5*IQR)

    print(f"Old Shape from {col}: {dataMobile_noDuplicate.shape}")
    dataMobile_noDuplicate.drop(dataMobile_noDuplicate[
        (dataMobile_noDuplicate[col] > RUB) 
        | 
        (dataMobile_noDuplicate[col] < RLB)].index, 
        inplace=True)
    print(f"New Shape from {col}: {dataMobile_noDuplicate.shape}\n")